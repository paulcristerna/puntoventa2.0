﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace Punto_Venta
{
    class Venta
    {
        public static DataTable venta()
        {
            DataTable retorno = new DataTable();
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                //int Cantidad = 2; 
                
                string Consulta = "SELECT Codigo, Nombre, Descripcion, Precio FROM Productos";
                MySqlCommand Comando = new MySqlCommand(Consulta, conexion);
                MySqlDataAdapter Adaptador = new MySqlDataAdapter(Comando);
                Adaptador.Fill(retorno);

                DataColumn dc = retorno.Columns.Add("PrecioTotal", typeof(decimal));
                //dc.Expression = "[Precio]*[Precio]";
                //dc.Expression = "[Precio]"*  
            }

            return retorno;
        }

        /*
        public static int Agregar_Producto_Venta(Pro_Venta pProducto)
        {
            int retorno = 0;
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                MySqlCommand Comando = new MySqlCommand(string.Format("Insert Into VentaTrabajo (Codigo, Nombre, Descripcion, Cantidad, PrecioUnitario, PrecioTotal) values ('{0}','{1}','{2}','{3}','{4}','{5}')",
                    pProducto.Codigo, pProducto.Nombre, pProducto.Descripcion, pProducto.Cantidad, pProducto.PrecioUnitario, pProducto.PrecioTotal), conexion);

                retorno = Comando.ExecuteNonQuery();
                conexion.Close();

            }
            return retorno;
        }
       */
        
        public static List<Pro_Venta> BuscarProductos_Codigo(Int64 pCodigo, Int16 pCantidad)
        {            
                Pro_Venta pProducto = new Pro_Venta();

                //preciototal.Expression = "[Precio]*[Cantidad]";  
                List<Pro_Venta> Lista = new List<Pro_Venta>();
                using (MySqlConnection conexion = Conexion.MiConexion())
                {
                        MySqlCommand comando = new MySqlCommand(string.Format(
                            //"Select Codigo, Nombre,  Descripcion, Precio from Productos where Codigo like '%{0}%' or Nombre like '%{1}%'", pCodigo, pNombre), conexion);
                            "Select Codigo, Nombre, Descripcion, Precio from Productos where Codigo={0}", pCodigo), conexion);

                        MySqlDataReader reader = comando.ExecuteReader();
                        
                        //MySqlDataAdapter Adaptador = new MySqlDataAdapter(comando);

                        while (reader.Read())
                        {

                            //Pro_Venta pProducto = new Pro_Venta();
                            pProducto.Codigo = reader.GetInt64(0);
                            pProducto.Nombre = reader.GetString(1);
                            pProducto.Descripcion = reader.GetString(2);
                            pProducto.Cantidad = Convert.ToInt16(pCantidad);
                            pProducto.Precio = reader.GetDouble(3);

                            double preciototal = pProducto.Precio * pProducto.Cantidad;
                            pProducto.PrecioTotal = Convert.ToDouble(preciototal);
                            //pProducto.Cantidad = Convert.ToInt16(cantidad);

                            Lista.Add(pProducto);
                        }                                          
                        conexion.Close();
                    
                        return Lista;                                           
                }           
        }

        public static int AgregarVenta(Pro_Venta pVenta)
        {
            int retorno = 0;
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                MySqlCommand Comando = new MySqlCommand(string.Format("Insert Into Ventas (Cliente, Total, PagoCon, Cambio, Estado) values ('{0}','{1}','{2}','{3}','A')",
                    pVenta.Cliente, pVenta.Total, pVenta.PagoCon, pVenta.Cambio), conexion);

                retorno = Comando.ExecuteNonQuery();
                conexion.Close();

            }
            return retorno;
        }

        public static int AgregarDetalleVenta(Pro_Venta pVenta)
        {
            int retorno = 0;
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                MySqlCommand Comando = new MySqlCommand(string.Format("Insert Into Detalle_Ventas (Codigo, Cantidad, PrecioUnitario) values ('{0}','{1}','{2}')",
                    pVenta.Codigo, pVenta.Cantidad, pVenta.Precio), conexion);

                retorno = Comando.ExecuteNonQuery();
                conexion.Close();

            }
            return retorno;
        }
    }
}
