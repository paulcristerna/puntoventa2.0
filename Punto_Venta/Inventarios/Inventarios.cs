﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Punto_Venta
{
    public partial class Inventarios : Form
    {
        public Inventarios()
        {
            InitializeComponent();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Menu_Principal Open = new Menu_Principal();
            Open.ShowDialog();
        }

        private void Inventarios_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Met_Productos.CargarProductos();
            txtCodigo.Focus();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            Modificar_Inventario Form = new Modificar_Inventario();
            Form.txtCodigo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[0].Value);
            Form.txtNombre.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[1].Value);
            Form.txtDescripcion.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[2].Value);
            Form.txtPrecio.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[3].Value);
            Form.txtStock.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[4].Value);
            this.Hide();
            Form.ShowDialog();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Buscar_Productos_Inventarios Abrir = new Buscar_Productos_Inventarios();
            this.Hide();
            Abrir.ShowDialog();
        }

        private void txtCodigo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.DataSource = Met_Productos.BuscarProductos_Codigo(Convert.ToInt64(txtCodigo.Text));
            }
            catch (Exception)
            {
                dataGridView1.DataSource = Met_Productos.CargarProductos();
            }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.DataSource = Met_Productos.BuscarProductos_Nombre(txtNombre.Text);
            }
            catch (Exception)
            {
                dataGridView1.DataSource = Met_Productos.CargarProductos();
            }
        }

        
    }
}
