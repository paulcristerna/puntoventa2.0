﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Punto_Venta
{
    public partial class Modificar_Inventario : Form
    {
        public Modificar_Inventario()
        {
            InitializeComponent();
        }

        private void btnModificar_Inventario_Click(object sender, EventArgs e)
        {
            try
            {
                Pro_Productos Pro_Productos = new Pro_Productos();
                if (txtStock.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo stock", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtStock.Focus();
                    return;
                }
                else
                    Pro_Productos.Codigo = Convert.ToInt64(txtCodigo.Text);
                    Pro_Productos.Nombre = txtNombre.Text;
                    Pro_Productos.Descripcion = txtDescripcion.Text;
                    Pro_Productos.Precio = Convert.ToDouble(txtPrecio.Text);
                    Pro_Productos.Stock = Convert.ToInt16(txtStock.Text);
                    Met_Productos.Modificar(Pro_Productos);
                    MessageBox.Show("Datos Modificados Corerectamente", "Datos Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpiar();
            }
            catch
            {
                MessageBox.Show("No se pudieron modificar lo datos", "Error al Guardar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        void limpiar()
        {
            txtCodigo.Clear();
            txtNombre.Clear();
            txtDescripcion.Clear();
            txtPrecio.Clear();
            txtStock.Clear();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Inventarios Abrir = new Inventarios();
            Abrir.ShowDialog();
        }

        private void txtStock_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Numeros(e);
        }

    }
}
