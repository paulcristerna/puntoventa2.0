﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Punto_Venta
{
    public partial class Buscar_Productos_Inventarios : Form
    {
        public Buscar_Productos_Inventarios()
        {
            InitializeComponent();
        }

        private void btnBuscar_Producto_In_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtCodigo.Text))
            {
                dataGridView1.DataSource = Met_Productos.BuscarProductos_Nombre(txtNombre.Text);
            }
            else if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                dataGridView1.DataSource = Met_Productos.BuscarProductos_Codigo(Convert.ToInt64(txtCodigo.Text));
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Inventarios Abrir = new Inventarios();
            Abrir.ShowDialog();
        }
    }
}
