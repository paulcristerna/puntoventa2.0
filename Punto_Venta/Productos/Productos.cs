﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Punto_Venta
{
    public partial class Productos : Form
    {
        public Productos()
        {
            InitializeComponent();
        }

        private void btnRegistrar_Producto_Click(object sender, EventArgs e)
        {
            this.Hide();
            Registro_Producto Abrir = new Registro_Producto();
            Abrir.ShowDialog();
        }

        private void btnModificar_Producto_Click(object sender, EventArgs e)
        {
            Modificar_Producto Form = new Modificar_Producto();
            Form.txtCodigo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[0].Value);
            Form.txtNombre.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[1].Value);
            Form.txtDescripcion.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[2].Value);
            Form.txtPrecio.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[3].Value);
            Form.txtStock.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[4].Value);
            Form.cmbTipo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[5].Value);
            this.Hide();
            Form.ShowDialog();
        }

        private void btnEliminar_Producto_Click(object sender, EventArgs e)
        {
            //Int64 Codigo = Convert.ToInt64(dataGridView1.CurrentRow.Cells[0].Value);
            //ProductoSeleccionado = Met_Productos.ObtenerProducto(Codigo);
            //this.Close();
            Eliminar_Producto Form = new Eliminar_Producto();
            Form.txtCodigo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[0].Value);
            Form.txtNombre.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[1].Value);
            Form.txtDescripcion.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[2].Value);
            Form.txtPrecio.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[3].Value);
            Form.txtStock.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[4].Value);
            Form.cmbTipo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[5].Value);
            this.Hide();
            Form.ShowDialog();
        }

        private void btnBuscar_Producto_Click(object sender, EventArgs e)
        {
            Buscar_Producto Abrir = new Buscar_Producto();
            this.Hide();
            Abrir.ShowDialog();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Menu_Principal Abrir = new Menu_Principal();
            Abrir.ShowDialog();
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {

        }

        private void Productos_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Met_Productos.CargarProductos();
            txtCodigo.Focus();
        }

        private void txtCodigo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.DataSource = Met_Productos.BuscarProductos_Codigo(Convert.ToInt64(txtCodigo.Text));
            }
            catch (Exception)
            {
                dataGridView1.DataSource = Met_Productos.CargarProductos();
            }    
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {     
            /*
            try
            {
                dataGridView1.DataSource = Met_Productos.BuscarProductos_Nombre(txtNombre.Text);
            }
            catch (Exception)
            {
                dataGridView1.DataSource = Met_Productos.CargarProductos();
            }
             */ 
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            dataGridView1.DataSource = Met_Productos.BuscarProductos_Nombre(txtNombre.Text);           
        }                     
    }
}
