﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices; //Servicio para activar user32.dll

namespace Punto_Venta
{
    public partial class Registro_Producto : Form
    {
        //Servicio de PlaceHolder en textboxs
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern Int32
                SendMessage(
                                IntPtr hWnd,
                                int msg,
                                int wParam,
                                [MarshalAs(UnmanagedType.LPWStr)]string lParam
                            );
        private const int EM_SETCUEBANNER = 0x1501;        

        public Registro_Producto()
        {
            InitializeComponent();
            //Aqui se definen los mensajes que apareceran en los textboxs de placeholder
            SendMessage(txtCodigo.Handle, EM_SETCUEBANNER, 0, "Codigo de producto");
            SendMessage(txtNombre.Handle, EM_SETCUEBANNER, 0, "Nombre de producto");
            SendMessage(txtDescripcion.Handle, EM_SETCUEBANNER, 0, "Descripcion");
            SendMessage(txtPrecio.Handle, EM_SETCUEBANNER, 0, "Precio");
            SendMessage(txtStock.Handle, EM_SETCUEBANNER, 0, "Stock");
            //txtNombreUsuario.Text = "asdadadad";
        }

        private void btnGuardar_Producto_Click(object sender, EventArgs e)
        {
            try
            {
            Pro_Productos Pro_Productos = new Pro_Productos();
            if (txtCodigo.Text.Length == 0)   
            {
                MessageBox.Show("Error en campo codigo","Campos Obligatorio, verifique",MessageBoxButtons.OK,MessageBoxIcon.Error);
                txtCodigo.Focus();
                return;                       
            }
            if (txtNombre.Text.Length == 0)
            {
                MessageBox.Show("Error en campo nombre", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtNombre.Focus();
                return;
            }
            if (txtDescripcion.Text.Length == 0)
            {
                MessageBox.Show("Error en campo descripcion", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDescripcion.Focus();
                return;
            }
            if (txtPrecio.Text.Length == 0)
            {
                MessageBox.Show("Error en campo precio", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPrecio.Focus();
                return;
            }
            if (txtStock.Text.Length == 0)
            {
                MessageBox.Show("Error en campo stock", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtStock.Focus();
                return;
            }

            else

                Pro_Productos.Codigo = Convert.ToInt64(txtCodigo.Text);
                Pro_Productos.Nombre = txtNombre.Text;
                Pro_Productos.Descripcion = txtDescripcion.Text;
                Pro_Productos.Precio = Convert.ToDouble(txtPrecio.Text);
                Pro_Productos.Stock = Convert.ToInt16(txtStock.Text);
                Pro_Productos.TipoUnidad = cmbTipo.Text;
                Met_Productos.Agregar(Pro_Productos);
                MessageBox.Show("Datos Guardados Corerectamente", "Datos Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiar();

            }
            catch
            {
                MessageBox.Show("No se pudieron Guardar lo datos", "Error al Guardar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);           
            }            
        }

        void limpiar()
        {
            txtCodigo.Clear();
            txtNombre.Clear();
            txtDescripcion.Clear();
            txtPrecio.Clear();
            txtStock.Clear();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Productos Abrir = new Productos();
            Abrir.ShowDialog();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Numeros(e);
        }

        private void txtPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Numeros(e);
        }

        private void txtStock_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Numeros(e);
        }       
    }
}
