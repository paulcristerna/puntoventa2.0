﻿namespace Punto_Venta
{
    partial class Productos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Productos));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnRegistrar_Producto = new System.Windows.Forms.Button();
            this.btnModificar_Producto = new System.Windows.Forms.Button();
            this.btnEliminar_Producto = new System.Windows.Forms.Button();
            this.btnBuscar_Producto = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAtras = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.lblProducto = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 240);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(700, 390);
            this.dataGridView1.TabIndex = 0;
            // 
            // btnRegistrar_Producto
            // 
            this.btnRegistrar_Producto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRegistrar_Producto.BackgroundImage")));
            this.btnRegistrar_Producto.Location = new System.Drawing.Point(1, 2);
            this.btnRegistrar_Producto.Name = "btnRegistrar_Producto";
            this.btnRegistrar_Producto.Size = new System.Drawing.Size(128, 128);
            this.btnRegistrar_Producto.TabIndex = 1;
            this.btnRegistrar_Producto.UseVisualStyleBackColor = true;
            this.btnRegistrar_Producto.Click += new System.EventHandler(this.btnRegistrar_Producto_Click);
            // 
            // btnModificar_Producto
            // 
            this.btnModificar_Producto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnModificar_Producto.BackgroundImage")));
            this.btnModificar_Producto.Location = new System.Drawing.Point(135, 2);
            this.btnModificar_Producto.Name = "btnModificar_Producto";
            this.btnModificar_Producto.Size = new System.Drawing.Size(128, 128);
            this.btnModificar_Producto.TabIndex = 2;
            this.btnModificar_Producto.UseVisualStyleBackColor = true;
            this.btnModificar_Producto.Click += new System.EventHandler(this.btnModificar_Producto_Click);
            // 
            // btnEliminar_Producto
            // 
            this.btnEliminar_Producto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEliminar_Producto.BackgroundImage")));
            this.btnEliminar_Producto.Location = new System.Drawing.Point(269, 2);
            this.btnEliminar_Producto.Name = "btnEliminar_Producto";
            this.btnEliminar_Producto.Size = new System.Drawing.Size(128, 128);
            this.btnEliminar_Producto.TabIndex = 3;
            this.btnEliminar_Producto.UseVisualStyleBackColor = true;
            this.btnEliminar_Producto.Click += new System.EventHandler(this.btnEliminar_Producto_Click);
            // 
            // btnBuscar_Producto
            // 
            this.btnBuscar_Producto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBuscar_Producto.BackgroundImage")));
            this.btnBuscar_Producto.Location = new System.Drawing.Point(403, 2);
            this.btnBuscar_Producto.Name = "btnBuscar_Producto";
            this.btnBuscar_Producto.Size = new System.Drawing.Size(128, 128);
            this.btnBuscar_Producto.TabIndex = 4;
            this.btnBuscar_Producto.UseVisualStyleBackColor = true;
            this.btnBuscar_Producto.Click += new System.EventHandler(this.btnBuscar_Producto_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAyuda.BackgroundImage")));
            this.btnAyuda.Location = new System.Drawing.Point(823, 2);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(128, 128);
            this.btnAyuda.TabIndex = 5;
            this.btnAyuda.UseVisualStyleBackColor = true;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(728, 240);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(223, 217);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // btnAtras
            // 
            this.btnAtras.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAtras.BackgroundImage")));
            this.btnAtras.Location = new System.Drawing.Point(689, 2);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(128, 128);
            this.btnAtras.TabIndex = 7;
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 193);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(319, 26);
            this.label4.TabIndex = 36;
            this.label4.Text = "Busqueda por nombre del producto";
            // 
            // txtNombre
            // 
            this.txtNombre.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(345, 190);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(226, 33);
            this.txtNombre.TabIndex = 35;
            this.txtNombre.TextChanged += new System.EventHandler(this.txtNombre_TextChanged);
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // txtCodigo
            // 
            this.txtCodigo.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigo.Location = new System.Drawing.Point(345, 145);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(226, 33);
            this.txtCodigo.TabIndex = 34;
            this.txtCodigo.TextChanged += new System.EventHandler(this.txtCodigo_TextChanged);
            // 
            // lblProducto
            // 
            this.lblProducto.AutoSize = true;
            this.lblProducto.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProducto.Location = new System.Drawing.Point(20, 148);
            this.lblProducto.Name = "lblProducto";
            this.lblProducto.Size = new System.Drawing.Size(229, 26);
            this.lblProducto.TabIndex = 33;
            this.lblProducto.Text = "Escanee codigo de barras";
            // 
            // Productos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 642);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.lblProducto);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnAyuda);
            this.Controls.Add(this.btnBuscar_Producto);
            this.Controls.Add(this.btnEliminar_Producto);
            this.Controls.Add(this.btnModificar_Producto);
            this.Controls.Add(this.btnRegistrar_Producto);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Productos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Productos";
            this.Load += new System.EventHandler(this.Productos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnRegistrar_Producto;
        private System.Windows.Forms.Button btnModificar_Producto;
        private System.Windows.Forms.Button btnEliminar_Producto;
        private System.Windows.Forms.Button btnBuscar_Producto;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label lblProducto;
    }
}