﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Punto_Venta
{
    public partial class Modificar_Producto : Form
    {
        public Modificar_Producto()
        {
            InitializeComponent();
        }       
        private void btnModificar_Producto_Click(object sender, EventArgs e)
        {
            try
            {
                Pro_Productos Pro_Productos = new Pro_Productos();
                if (txtNombre.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo nombre", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtNombre.Focus();
                    return;
                }
                if (txtDescripcion.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo descripcion", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDescripcion.Focus();
                    return;
                }
                if (txtPrecio.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo precio", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPrecio.Focus();
                    return;
                }
                else
                    Pro_Productos.Codigo = Convert.ToInt64(txtCodigo.Text);
                    Pro_Productos.Nombre = txtNombre.Text;
                    Pro_Productos.Descripcion = txtDescripcion.Text;
                    Pro_Productos.Precio = Convert.ToDouble(txtPrecio.Text);
                    Pro_Productos.Stock = Convert.ToInt16(txtStock.Text);
                    Pro_Productos.TipoUnidad = cmbTipo.Text;
                    Met_Productos.Modificar(Pro_Productos);
                    MessageBox.Show("Datos Modificados Corerectamente", "Datos Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpiar();
            }
            catch
            {
                MessageBox.Show("No se pudieron modificar lo datos", "Error al Guardar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        void limpiar()
        {
            txtCodigo.Clear();
            txtNombre.Clear();
            txtDescripcion.Clear();
            txtPrecio.Clear();
            txtStock.Clear();
            cmbTipo.Text = ("Tipo Unidad");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Productos Abrir = new Productos();
            Abrir.ShowDialog();
        }

        private void txtPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Numeros(e);
        }
    }
}
