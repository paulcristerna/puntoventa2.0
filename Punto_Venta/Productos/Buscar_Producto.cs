﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices; //Servicio para activar user32.dll

namespace Punto_Venta
{
    public partial class Buscar_Producto : Form
    {
        //Servicio de PlaceHolder en textboxs
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern Int32
                SendMessage(
                                IntPtr hWnd,
                                int msg,
                                int wParam,
                                [MarshalAs(UnmanagedType.LPWStr)]string lParam
                            );
        private const int EM_SETCUEBANNER = 0x1501;

        public Buscar_Producto()
        {
            InitializeComponent();
            //Aqui se definen los mensajes que apareceran en los textboxs de placeholder
            SendMessage(txtCodigo.Handle, EM_SETCUEBANNER, 0, "Ingrese el Codigo de producto");
            SendMessage(txtNombre.Handle, EM_SETCUEBANNER, 0, "Ingrese el Nombre de producto");
        }

        private void btnBuscar_Producto_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtCodigo.Text))
            {
                dataGridView1.DataSource = Met_Productos.BuscarProductos_Nombre(txtNombre.Text);
            }
            else if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                dataGridView1.DataSource = Met_Productos.BuscarProductos_Codigo(Convert.ToInt64(txtCodigo.Text));
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Productos Abrir = new Productos();
            Abrir.ShowDialog();
        } 
    }
}
