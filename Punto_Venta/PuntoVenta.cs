﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices; //Servicio para activar user32.dll
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace Punto_Venta
{
    public partial class PuntoVenta : Form
    {
        //Servicio de PlaceHolder en textboxs
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern Int32
                SendMessage(
                                IntPtr hWnd,
                                int msg,
                                int wParam,
                                [MarshalAs(UnmanagedType.LPWStr)]string lParam
                            );
        private const int EM_SETCUEBANNER = 0x1501;

        DataTable DTProducto = new DataTable();

        public PuntoVenta()
        {
            InitializeComponent();
            //Aqui se definen los mensajes que apareceran en los textboxs de placeholder
            SendMessage(txtCliente.Handle, EM_SETCUEBANNER, 0, "Publico en general");
            //SendMessage(txtNombre.Handle, EM_SETCUEBANNER, 0, "Ingrese codigo de producto");
            //SendMessage(txtNombre.Handle, EM_SETCUEBANNER, 0, "Busque nombre del producto");
            //txtNombreUsuario.Text = "asdadadad";
        }

        //autocomplete

        private void DatosProductos()
        {
            Met_Productos producto = new Met_Productos();
            
            producto.Consultar(DTProducto);
        } 


        private void ActualizarAutoCompletes()
        {
            txtNombre.AutoCompleteCustomSource = ListadoProductos();
            txtNombre.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtNombre.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }

        public AutoCompleteStringCollection ListadoProductos()
        {
            string Dato;
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            //recorrer y cargar los items para el autocompletado
            foreach (DataRow row in DTProducto.Rows)
            {
                Dato = Convert.ToString(row["Nombre"]) + " " + Convert.ToString(row["Descripcion"]);
                coleccion.Add(Dato);
            }

            return coleccion;
        }

        private void btnPagar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btnAgregarPago, "Ingresar pago a la caja");
        }

        private void btnAgregar_Carrito_Click(object sender, EventArgs e)
        {
            if (txtProducto.Text.Length == 0 && txtNombre.Text.Length == 0)
            {
                MessageBox.Show("Error en campo codigo o nombre estan vacios", "Campo Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtProducto.Focus();
                return;
            }
            else if (txtProducto.Text.Length != 0 && txtNombre.Text.Length == 0)
            {
                double preciototal = 0;
                MySqlConnection conexion = Conexion.MiConexion();
                MySqlDataAdapter ada = new MySqlDataAdapter("Select * from MiniSuper.Productos where Codigo='" + this.txtProducto.Text + "';", conexion);

                DataTable dt = new DataTable();
                ada.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["Codigo"].ToString());
                    listitem.SubItems.Add(dr["Nombre"].ToString());
                    listitem.SubItems.Add(dr["Descripcion"].ToString());
                    listitem.SubItems.Add(dr["TipoUnidad"].ToString());
                    listitem.SubItems.Add(dr["Precio"].ToString());
                    listitem.SubItems.Add(txtCantidad.Text);
                    preciototal = double.Parse(listitem.SubItems[4].Text) * Convert.ToDouble(txtCantidad.Text);

                    //Control del stock
                    double stock = Convert.ToDouble(dr["Stock"].ToString());
                    double stock_control = double.Parse(stock.ToString()) - Convert.ToDouble(txtCantidad.Text);
                    Pro_Productos Pro_Productos = new Pro_Productos();
                    Pro_Productos.Codigo = Convert.ToInt64(dr["Codigo"].ToString());
                    Pro_Productos.Stock = Convert.ToDouble(stock_control);
                    Met_Productos.Modificar_Stock(Pro_Productos);

                    listitem.SubItems.Add(preciototal.ToString());
                    listView1.Items.Add(listitem);
                }

                double Total = 0;
                foreach (ListViewItem I in listView1.Items)
                {
                    Total += double.Parse(I.SubItems[6].Text);
                }
                txtTotal.Text = Total.ToString();
                txtProducto.Clear();
                txtNombre.Text = "";
                txtCantidad.Text = "1";
                txtProducto.Focus();
            }
            else if (txtProducto.Text.Length == 0 && txtNombre.Text.Length != 0)
            {
                double preciototal = 0;
                MySqlConnection conexion = Conexion.MiConexion();
                MySqlDataAdapter ada = new MySqlDataAdapter("Select * from Productos where concat_ws(' ',Nombre,Descripcion) LIKE '%" + this.txtNombre.Text + "%';", conexion);

                DataTable dt = new DataTable();
                ada.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["Codigo"].ToString());
                    listitem.SubItems.Add(dr["Nombre"].ToString());
                    listitem.SubItems.Add(dr["Descripcion"].ToString());
                    listitem.SubItems.Add(dr["TipoUnidad"].ToString());
                    listitem.SubItems.Add(dr["Precio"].ToString());
                    listitem.SubItems.Add(txtCantidad.Text);
                    preciototal = double.Parse(listitem.SubItems[4].Text) * Convert.ToDouble(txtCantidad.Text);

                    //Control del stock
                    double stock = Convert.ToDouble(dr["Stock"].ToString());
                    double stock_control = double.Parse(stock.ToString()) - Convert.ToDouble(txtCantidad.Text);
                    Pro_Productos Pro_Productos = new Pro_Productos();
                    Pro_Productos.Codigo = Convert.ToInt64(dr["Codigo"].ToString());
                    Pro_Productos.Stock = Convert.ToDouble(stock_control);
                    Met_Productos.Modificar_Stock(Pro_Productos);

                    listitem.SubItems.Add(preciototal.ToString());
                    listView1.Items.Add(listitem);
                }

                double Total = 0;
                foreach (ListViewItem I in listView1.Items)
                {
                    Total += double.Parse(I.SubItems[6].Text);
                }
                txtTotal.Text = Total.ToString();
                txtProducto.Clear();
                txtNombre.Clear();
                txtCantidad.Text = "1";
                txtProducto.Focus();
            }
            else 
            {
                MessageBox.Show("Error en campo codigo y nombre no deben estar llenos en el mismo formulario", "Campo Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtProducto.Focus();
                return;
            }
        }       

        private void PuntoVenta_Load(object sender, EventArgs e)
        {
            tssUsuarioVenta.Text = "Usuario: " + Form1.user;
            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;
            txtProducto.Focus();

            DatosProductos();
            ActualizarAutoCompletes();
        }

        private void btnAgregarPago_Click(object sender, EventArgs e)
        {
            if (txtPagoCon.Text.Length == 0)
            {
                MessageBox.Show("Error en campo pagon con", "Campo Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPagoCon.Focus();
                return;
            }
            else if (Convert.ToDouble(txtPagoCon.Text) < Convert.ToDouble(txtTotal.Text))
            {
                MessageBox.Show("El campo de pago con debe ser mayor o igual al total", "Error en campo pago con, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPagoCon.Focus();
                return;
            }

            //guardar venta
            Pro_Venta Pro_Venta = new Pro_Venta();
            Pro_Venta.Cliente = txtCliente.Text;
            Pro_Venta.Total = Convert.ToDouble(txtTotal.Text);
            Pro_Venta.PagoCon= Convert.ToDouble(txtPagoCon.Text);
            
            //sacar cambio
            double total = double.Parse(txtPagoCon.Text) - double.Parse(txtTotal.Text);
            txtCambio.Text = total.ToString();
            Pro_Venta.Cambio = Convert.ToDouble(txtCambio.Text);

            int resultado = Venta.AgregarVenta(Pro_Venta);

            if (resultado > 0)
            {
                //MessageBox.Show("Datos Guardados Correctamente", "Datos Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //limpiar();
            }
            else
            {
                MessageBox.Show("No se pudieron Guardar lo datos", "Error al Guardar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            //guardar detalle de las ventas
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                Pro_Venta.Codigo = Convert.ToInt64(listView1.Items[i].SubItems[0].Text);
                Pro_Venta.Cantidad = Convert.ToDouble(listView1.Items[i].SubItems[5].Text);
                Pro_Venta.Precio = Convert.ToDouble(listView1.Items[i].SubItems[4].Text);

                Venta.AgregarDetalleVenta(Pro_Venta);
            }

            //generar ticket           
            
            Ticket ticket = new Ticket();
            ticket.AddHeaderLine("Ferreteria y Materiales");
            ticket.AddHeaderLine("Del Rio");
            ticket.AddHeaderLine("Villa Toledo #2719");
            ticket.AddHeaderLine("Culiacan, Sin");
            ticket.AddHeaderLine("RFC: VILA-841113-3R4");
            //ticket.AddSubHeaderLine("Ticket # 1");
            ticket.AddSubHeaderLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());

            for (int i = 0; i < listView1.Items.Count; i++)
            {
                //ticket.AddItem(FormPadre.listView1.Items[4].Text, FormPadre.listView1.Items[1].Text, FormPadre.listView1.Items[5].Text);
                ticket.AddItem(listView1.Items[i].SubItems[5].Text, listView1.Items[i].SubItems[1].Text, listView1.Items[i].SubItems[6].Text);
            }

            //ticket.AddTotal("SubTotal", txtSubTotal.Text);
            //ticket.AddTotal("IVA", txtIVA.Text);
            //Add total utiliza dos variables para poder funcionar
            ticket.AddTotal("Total", txtTotal.Text); //Ponemos un total en blanco que sirve de espacio
            ticket.AddTotal("Pago Con", txtPagoCon.Text);
            ticket.AddTotal("Cambio", txtCambio.Text);
            //ticket.AddHeaderLine("Le atendio", tssUsuarioVenta.Text);
            ticket.AddFooterLine("GRACIAS POR TU VISITA");
            //ticket.PrintTicket("Printer");
            ticket.PrintTicket("EPSON TM-U220 Receipt");
            

            //limpiar campos
            /*
            listView1.Items.Clear();
            txtTotal.Clear();
            txtPagoCon.Clear();
            txtCambio.Clear();
            txtNombre.Clear();
            txtProducto.Focus();
            */ 
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Menu_Principal Open = new Menu_Principal();
            Open.ShowDialog();
        }

        private void txtProducto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                double preciototal = 0;
                MySqlConnection conexion = Conexion.MiConexion();
                MySqlDataAdapter ada = new MySqlDataAdapter("Select * from MiniSuper.Productos where Codigo='" + this.txtProducto.Text + "';", conexion);
                //MySqlDataAdapter stock = new MySqlDataAdapter("Update * from MiniSuper.Productos where Codigo='" + this.txtProducto.Text + "' set Stock=Stock - '" + this.txtCantidad.Text + "';", conexion);
                //MySqlDataAdapter ada = new MySqlDataAdapter("select Codigo, Nombre, Descripcion, Precio from Productos where Codigo=", txtProducto.Text, conexion);
                DataTable dt = new DataTable();
                ada.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["Codigo"].ToString());
                    listitem.SubItems.Add(dr["Nombre"].ToString());
                    listitem.SubItems.Add(dr["Descripcion"].ToString());
                    listitem.SubItems.Add(dr["TipoUnidad"].ToString());
                    listitem.SubItems.Add(dr["Precio"].ToString());
                    listitem.SubItems.Add(txtCantidad.Text);
                    preciototal = double.Parse(listitem.SubItems[4].Text) * Convert.ToDouble(txtCantidad.Text);

                    //Control del stock
                    double stock2 = Convert.ToDouble(dr["Stock"].ToString());
                    double stock_control = int.Parse(stock2.ToString()) - Convert.ToDouble(txtCantidad.Text);
                    Pro_Productos Pro_Productos = new Pro_Productos();
                    Pro_Productos.Codigo = Convert.ToInt64(dr["Codigo"].ToString());
                    Pro_Productos.Stock = Convert.ToDouble(stock_control);
                    Met_Productos.Modificar_Stock(Pro_Productos);

                    listitem.SubItems.Add(preciototal.ToString());
                    listView1.Items.Add(listitem);
                }

                double Total = 0;
                foreach (ListViewItem I in listView1.Items)
                {
                    Total += double.Parse(I.SubItems[6].Text);
                }
                txtTotal.Text = Total.ToString();
                txtProducto.Clear();
                txtNombre.Clear();
                txtCantidad.Text = "1";
                txtProducto.Focus();
            }                       
        }

        private void btnNuevaVenta_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            txtTotal.Clear();
            txtPagoCon.Clear();
            txtCambio.Clear();
            txtProducto.Clear();
            txtNombre.Clear();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DateTime.Now.ToShortDateString().ToString() + " - " + DateTime.Now.ToLongTimeString().ToString(); 
        }

        private void txtPagoCon_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Numeros(e);
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                double preciototal = 0;
                MySqlConnection conexion = Conexion.MiConexion();
                MySqlDataAdapter ada = new MySqlDataAdapter("Select * from Productos where concat_ws(' ',Nombre,Descripcion) LIKE '%" + this.txtNombre.Text + "%';", conexion);

                DataTable dt = new DataTable();
                ada.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["Codigo"].ToString());
                    listitem.SubItems.Add(dr["Nombre"].ToString());
                    listitem.SubItems.Add(dr["Descripcion"].ToString());
                    listitem.SubItems.Add(dr["TipoUnidad"].ToString());
                    listitem.SubItems.Add(dr["Precio"].ToString());
                    listitem.SubItems.Add(txtCantidad.Text);
                    preciototal = double.Parse(listitem.SubItems[4].Text) * Convert.ToDouble(txtCantidad.Text);

                    //Control del stock
                    double stock = Convert.ToDouble(dr["Stock"].ToString());
                    double stock_control = double.Parse(stock.ToString()) - Convert.ToDouble(txtCantidad.Text);
                    Pro_Productos Pro_Productos = new Pro_Productos();
                    Pro_Productos.Codigo = Convert.ToInt64(dr["Codigo"].ToString());
                    Pro_Productos.Stock = Convert.ToDouble(stock_control);
                    Met_Productos.Modificar_Stock(Pro_Productos);

                    listitem.SubItems.Add(preciototal.ToString());
                    listView1.Items.Add(listitem);
                }

                double Total = 0;
                foreach (ListViewItem I in listView1.Items)
                {
                    Total += double.Parse(I.SubItems[6].Text);
                }
                txtTotal.Text = Total.ToString();
                txtProducto.Clear();
                txtNombre.Clear();
                txtCantidad.Text = "1";
                txtProducto.Focus();
            }
        }
        
    }
}
