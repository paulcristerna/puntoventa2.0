﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace Punto_Venta
{
    class Met_Usuarios
    {
        public static DataTable CargarUsuarios()
        {
            DataTable retorno = new DataTable();
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                string ConsultaUsuarios = "SELECT * FROM Usuarios";
                MySqlCommand Comando = new MySqlCommand(ConsultaUsuarios, conexion);
                MySqlDataAdapter Adaptador = new MySqlDataAdapter(Comando);
                Adaptador.Fill(retorno);
            }

            return retorno;
        }

        public static int Buscar (String NombreUsuario, String Contrasena)
        {
            int resultado = -1;
            MySqlConnection conexion = Conexion.MiConexion();
            MySqlCommand comando = new MySqlCommand(string.Format(
           "SELECT * FROM Usuarios where Usuario ='{0}' and Contrasena ='{1}'", NombreUsuario, Contrasena), conexion);
            MySqlDataReader reader = comando.ExecuteReader();
            while (reader.Read())
            {
                resultado = 50;
            }
            conexion.Close();
            return resultado;
        }

        public static string BuscarTipo(String NombreUsuario)
        {
            MySqlConnection conexion = Conexion.MiConexion();
            MySqlCommand comando = new MySqlCommand(string.Format(
           "SELECT Tipo FROM Usuarios where Usuario ='{0}' LIMIT 1", NombreUsuario), conexion);
            MySqlDataReader reader = comando.ExecuteReader();
            string Tipo = "";

            while (reader.Read())
            {
                Tipo = reader.GetString(0);
            }
            conexion.Close();
            return Tipo;
        }

        public static int Agregar(Pro_Usuarios pUsuario)
        {
            int retorno = 0;
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                MySqlCommand Comando = new MySqlCommand(string.Format("Insert Into Usuarios (Nombre, Apellido, Direccion, Usuario, Contrasena, Tipo) values ('{0}','{1}','{2}','{3}','{4}','{5}')",
                    pUsuario.Nombre, pUsuario.Apellido, pUsuario.Direccion, pUsuario.Usuario, pUsuario.Contrasena, pUsuario.Tipo), conexion);

                retorno = Comando.ExecuteNonQuery();
                conexion.Close();

            }
            return retorno;
        }

        public static int Modificar(Pro_Usuarios pUsuario)
        {
            int retorno = 0;
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                MySqlCommand comando = new MySqlCommand(string.Format("Update Usuarios set Nombre='{0}', Apellido='{1}', Direccion='{2}', Contrasena='{3}', Tipo='{4}' where Usuario='{5}'",
                    pUsuario.Nombre, pUsuario.Apellido, pUsuario.Direccion, pUsuario.Contrasena, pUsuario.Tipo, pUsuario.Usuario), conexion);

                retorno = comando.ExecuteNonQuery();
                conexion.Close();
            }
            return retorno;
        }

        public static int Eliminar(String pUsuario)
        {
            int retorno = 0;
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                MySqlCommand comando = new MySqlCommand(string.Format("Delete from Usuarios where Usuario='{0}'", pUsuario), conexion);
                retorno = comando.ExecuteNonQuery();
                conexion.Close();
            }
            return retorno;
        }
    }
}



