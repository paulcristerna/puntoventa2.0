﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Punto_Venta
{
    public partial class Modificar_Usuario : Form
    {
        public Modificar_Usuario()
        {
            InitializeComponent();
        }

        private void btnModificar_Usuario_Click(object sender, EventArgs e)
        {
            try
            {
                Pro_Usuarios Pro_Usuarios = new Pro_Usuarios();
                if (txtNombre_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo nombre", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtNombre_Mo.Focus();
                    return;
                }
                if (txtApellido_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo apellido", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtApellido_Mo.Focus();
                    return;
                }
                if (txtDireccion_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo direccion", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDireccion_Mo.Focus();
                    return;
                }
                if (txtContrasena_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo telefono de casa", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtContrasena_Mo.Focus();
                    return;
                }
                if (txtContrasena_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo telefono de casa", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtContrasena_Mo.Focus();
                    return;
                }
                if (cmbTipo_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo telefono de casa", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cmbTipo_Mo.Focus();
                    return;
                }
                else
                    Pro_Usuarios.Nombre = txtNombre_Mo.Text;
                    Pro_Usuarios.Apellido = txtApellido_Mo.Text;
                    Pro_Usuarios.Direccion = txtDireccion_Mo.Text;
                    Pro_Usuarios.Usuario = txtNombreUsuario_Mo.Text;
                    Pro_Usuarios.Contrasena = txtContrasena_Mo.Text;
                    Pro_Usuarios.Tipo = cmbTipo_Mo.Text;
                    Met_Usuarios.Modificar(Pro_Usuarios);
                    MessageBox.Show("Datos Guardados Corerectamente", "Datos Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpiar();
            }
            catch
            {
                MessageBox.Show("No se pudieron Guardar lo datos", "Error al Guardar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        void limpiar()
        {
            txtNombre_Mo.Clear();
            txtApellido_Mo.Clear();
            txtDireccion_Mo.Clear();
            txtNombreUsuario_Mo.Clear();
            txtContrasena_Mo.Clear();
            cmbTipo_Mo.Text = "Tipo";
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Usuarios Abrir = new Usuarios();
            Abrir.ShowDialog();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtNombre_Mo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Letras(e);
        }

        private void txtApellido_Mo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Letras(e);
        }
    }
}
