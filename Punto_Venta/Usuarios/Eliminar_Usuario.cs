﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Punto_Venta
{
    public partial class Eliminar_Usuario : Form
    {
        public Eliminar_Usuario()
        {
            InitializeComponent();
        }

        private void btnEliminar_Usuario_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Esta seguro que desea eliminar el usuario??", "Esta seguro?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                int resultado = Met_Usuarios.Eliminar(txtNombreUsuario.Text);

                if (resultado > 0)
                {

                    MessageBox.Show("Usuario Eliminado Correctamente", "Usuario Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Hide();
                    Usuarios Abrir = new Usuarios();
                    Abrir.ShowDialog();
                    //limpiar();
                    //btnEliminar.Enabled = false;
                    //btnModificar.Enabled = false;
                    //btnGuardar.Enabled = true;
                }

                else
                {
                    MessageBox.Show("No se pudo Eliminar el usuario", "Ocurrio un error!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
                MessageBox.Show("Se cancelo la eliminacion", "Cancelado");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Usuarios Abrir = new Usuarios();
            Abrir.ShowDialog();
        }
    }
}
