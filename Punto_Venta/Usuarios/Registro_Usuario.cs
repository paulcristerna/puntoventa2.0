﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices; //Servicio para activar user32.dll

namespace Punto_Venta
{
    public partial class Registro_Usuario : Form
    {
        //Servicio de PlaceHolder en textboxs
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern Int32
                SendMessage(
                                IntPtr hWnd,
                                int msg,
                                int wParam,
                                [MarshalAs(UnmanagedType.LPWStr)]string lParam
                            );
        private const int EM_SETCUEBANNER = 0x1501;

        public Registro_Usuario()
        {
            InitializeComponent();
            //Aqui se definen los mensajes que apareceran en los textboxs de placeholder
            SendMessage(txtNombre.Handle, EM_SETCUEBANNER, 0, "Nombre");
            SendMessage(txtApellido.Handle, EM_SETCUEBANNER, 0, "Apellido");
            SendMessage(txtDireccion.Handle, EM_SETCUEBANNER, 0, "Direccion");
            SendMessage(txtNombreUsuario.Handle, EM_SETCUEBANNER, 0, "Nombre de usuario");
            SendMessage(txtContrasena.Handle, EM_SETCUEBANNER, 0, "Contraseña");
            SendMessage(txtConfirmar_Contrasena.Handle, EM_SETCUEBANNER, 0, "Repite la contraseña");
            //SendMessage(cmbTipo.Handle, EM_SETCUEBANNER, 0, "Respuesta");
            //SendMessage(txtNombre.Handle, EM_SETCUEBANNER, 0, "Nombre de usuario");
            //txtNombreUsuario.Text = "asdadadad";
        }

        private void btnGuardar_Usuario_Click(object sender, EventArgs e)
        {
            try
            {
                Pro_Usuarios Pro_Usuarios = new Pro_Usuarios();
                if (txtNombre.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo nombre", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtNombre.Focus();
                    return;
                }
                if (txtApellido.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo apellido", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtApellido.Focus();
                    return;
                }
                if (txtDireccion.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo direccion", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDireccion.Focus();
                    return;
                }
                if (txtNombreUsuario.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo nombre de usuario", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtNombreUsuario.Focus();
                    return;
                }
                if (txtContrasena.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo telefono de casa", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtContrasena.Focus();
                    return;
                }
                if (txtConfirmar_Contrasena.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo celular", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtConfirmar_Contrasena.Focus();
                    return;
                }
                else
                    Pro_Usuarios.Nombre = txtNombre.Text;
                    Pro_Usuarios.Apellido = txtApellido.Text;
                    Pro_Usuarios.Direccion = txtDireccion.Text;
                    Pro_Usuarios.Usuario = txtNombreUsuario.Text;
                    Pro_Usuarios.Contrasena = txtContrasena.Text;
                    Pro_Usuarios.Tipo = cmbTipo.Text;
                    Met_Usuarios.Agregar(Pro_Usuarios);
                    MessageBox.Show("Datos Guardados Corerectamente", "Datos Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpiar();
            }
            catch
            {
                MessageBox.Show("No se pudieron Guardar lo datos", "Error al Guardar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        void limpiar()
        {
            txtNombre.Clear();
            txtApellido.Clear();
            txtDireccion.Clear();
            txtNombreUsuario.Clear();
            txtContrasena.Clear();
            txtConfirmar_Contrasena.Clear();
            cmbTipo.Text = "Tipo";
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Usuarios Abrir = new Usuarios();
            Abrir.ShowDialog();
        }

       
    }
}
