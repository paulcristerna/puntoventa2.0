﻿namespace Punto_Venta
{
    partial class Usuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Usuarios));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button5 = new System.Windows.Forms.Button();
            this.btnBuscar_Usuario = new System.Windows.Forms.Button();
            this.btnEliminar_Usuario = new System.Windows.Forms.Button();
            this.btnModificar_Usuario = new System.Windows.Forms.Button();
            this.btnAgregar_Usuario = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button6 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(729, 228);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(223, 217);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // button5
            // 
            this.button5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button5.BackgroundImage")));
            this.button5.Location = new System.Drawing.Point(824, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(128, 128);
            this.button5.TabIndex = 12;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // btnBuscar_Usuario
            // 
            this.btnBuscar_Usuario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBuscar_Usuario.BackgroundImage")));
            this.btnBuscar_Usuario.Location = new System.Drawing.Point(404, 2);
            this.btnBuscar_Usuario.Name = "btnBuscar_Usuario";
            this.btnBuscar_Usuario.Size = new System.Drawing.Size(128, 128);
            this.btnBuscar_Usuario.TabIndex = 11;
            this.btnBuscar_Usuario.UseVisualStyleBackColor = true;
            // 
            // btnEliminar_Usuario
            // 
            this.btnEliminar_Usuario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEliminar_Usuario.BackgroundImage")));
            this.btnEliminar_Usuario.Location = new System.Drawing.Point(270, 2);
            this.btnEliminar_Usuario.Name = "btnEliminar_Usuario";
            this.btnEliminar_Usuario.Size = new System.Drawing.Size(128, 128);
            this.btnEliminar_Usuario.TabIndex = 10;
            this.btnEliminar_Usuario.UseVisualStyleBackColor = true;
            this.btnEliminar_Usuario.Click += new System.EventHandler(this.btnEliminar_Usuario_Click);
            // 
            // btnModificar_Usuario
            // 
            this.btnModificar_Usuario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnModificar_Usuario.BackgroundImage")));
            this.btnModificar_Usuario.Location = new System.Drawing.Point(136, 2);
            this.btnModificar_Usuario.Name = "btnModificar_Usuario";
            this.btnModificar_Usuario.Size = new System.Drawing.Size(128, 128);
            this.btnModificar_Usuario.TabIndex = 9;
            this.btnModificar_Usuario.UseVisualStyleBackColor = true;
            this.btnModificar_Usuario.Click += new System.EventHandler(this.btnModificar_Usuario_Click);
            // 
            // btnAgregar_Usuario
            // 
            this.btnAgregar_Usuario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAgregar_Usuario.BackgroundImage")));
            this.btnAgregar_Usuario.Location = new System.Drawing.Point(2, 2);
            this.btnAgregar_Usuario.Name = "btnAgregar_Usuario";
            this.btnAgregar_Usuario.Size = new System.Drawing.Size(128, 128);
            this.btnAgregar_Usuario.TabIndex = 8;
            this.btnAgregar_Usuario.UseVisualStyleBackColor = true;
            this.btnAgregar_Usuario.Click += new System.EventHandler(this.btnAgregar_Usuario_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 145);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(700, 404);
            this.dataGridView1.TabIndex = 7;
            // 
            // button6
            // 
            this.button6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button6.BackgroundImage")));
            this.button6.Location = new System.Drawing.Point(690, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(128, 128);
            this.button6.TabIndex = 14;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // Usuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 561);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.btnBuscar_Usuario);
            this.Controls.Add(this.btnEliminar_Usuario);
            this.Controls.Add(this.btnModificar_Usuario);
            this.Controls.Add(this.btnAgregar_Usuario);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Usuarios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Usuarios";
            this.Load += new System.EventHandler(this.Usuarios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnBuscar_Usuario;
        private System.Windows.Forms.Button btnEliminar_Usuario;
        private System.Windows.Forms.Button btnModificar_Usuario;
        private System.Windows.Forms.Button btnAgregar_Usuario;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button6;
    }
}