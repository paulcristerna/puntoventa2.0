﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Punto_Venta
{
    public partial class Usuarios : Form
    {
        public Usuarios()
        {
            InitializeComponent();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Menu_Principal Open = new Menu_Principal();
            Open.ShowDialog();
        }

        private void Usuarios_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Met_Usuarios.CargarUsuarios();
        }

        private void btnAgregar_Usuario_Click(object sender, EventArgs e)
        {
            this.Hide();
            Registro_Usuario Open = new Registro_Usuario();
            Open.ShowDialog();
        }

        private void btnModificar_Usuario_Click(object sender, EventArgs e)
        {
            Modificar_Usuario Form = new Modificar_Usuario();
            Form.txtNombre_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[0].Value);
            Form.txtApellido_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[1].Value);
            Form.txtDireccion_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[2].Value);
            Form.txtNombreUsuario_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[3].Value);
            Form.txtContrasena_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[4].Value);
            Form.cmbTipo_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[5].Value);
            this.Hide();
            Form.ShowDialog();
        }

        private void btnEliminar_Usuario_Click(object sender, EventArgs e)
        {
            Eliminar_Usuario Form = new Eliminar_Usuario();
            Form.txtNombre.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[0].Value);
            Form.txtApellido.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[1].Value);
            Form.txtDireccion.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[2].Value);
            Form.txtNombreUsuario.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[3].Value);
            Form.txtContrasena.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[4].Value);
            Form.cmbTipo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[5].Value);
            this.Hide();
            Form.ShowDialog();
        }
    }
}
