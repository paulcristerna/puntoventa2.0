﻿namespace Punto_Venta
{
    partial class Modificar_Usuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Modificar_Usuario));
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtContrasena_Mo = new System.Windows.Forms.TextBox();
            this.btnAtras = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtNombreUsuario_Mo = new System.Windows.Forms.TextBox();
            this.txtDireccion_Mo = new System.Windows.Forms.TextBox();
            this.txtApellido_Mo = new System.Windows.Forms.TextBox();
            this.txtNombre_Mo = new System.Windows.Forms.TextBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnModificar_Usuario = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbTipo_Mo = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 217);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(127, 29);
            this.label6.TabIndex = 63;
            this.label6.Text = "Contraseña";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(207, 29);
            this.label5.TabIndex = 62;
            this.label5.Text = "Nombre de usuario";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 29);
            this.label4.TabIndex = 61;
            this.label4.Text = "Direccion";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 29);
            this.label3.TabIndex = 60;
            this.label3.Text = "Apellidos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 29);
            this.label2.TabIndex = 59;
            this.label2.Text = "Nombre";
            // 
            // txtContrasena_Mo
            // 
            this.txtContrasena_Mo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContrasena_Mo.Location = new System.Drawing.Point(220, 215);
            this.txtContrasena_Mo.Name = "txtContrasena_Mo";
            this.txtContrasena_Mo.Size = new System.Drawing.Size(191, 31);
            this.txtContrasena_Mo.TabIndex = 58;
            // 
            // btnAtras
            // 
            this.btnAtras.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAtras.BackgroundImage")));
            this.btnAtras.Location = new System.Drawing.Point(581, 9);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(128, 128);
            this.btnAtras.TabIndex = 57;
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAyuda.BackgroundImage")));
            this.btnAyuda.Location = new System.Drawing.Point(715, 9);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(128, 128);
            this.btnAyuda.TabIndex = 56;
            this.btnAyuda.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(620, 163);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(223, 217);
            this.pictureBox1.TabIndex = 55;
            this.pictureBox1.TabStop = false;
            // 
            // txtNombreUsuario_Mo
            // 
            this.txtNombreUsuario_Mo.Enabled = false;
            this.txtNombreUsuario_Mo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreUsuario_Mo.Location = new System.Drawing.Point(220, 178);
            this.txtNombreUsuario_Mo.Name = "txtNombreUsuario_Mo";
            this.txtNombreUsuario_Mo.Size = new System.Drawing.Size(191, 31);
            this.txtNombreUsuario_Mo.TabIndex = 54;
            // 
            // txtDireccion_Mo
            // 
            this.txtDireccion_Mo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDireccion_Mo.Location = new System.Drawing.Point(220, 141);
            this.txtDireccion_Mo.Name = "txtDireccion_Mo";
            this.txtDireccion_Mo.Size = new System.Drawing.Size(191, 31);
            this.txtDireccion_Mo.TabIndex = 53;
            // 
            // txtApellido_Mo
            // 
            this.txtApellido_Mo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellido_Mo.Location = new System.Drawing.Point(220, 103);
            this.txtApellido_Mo.Name = "txtApellido_Mo";
            this.txtApellido_Mo.Size = new System.Drawing.Size(191, 31);
            this.txtApellido_Mo.TabIndex = 52;
            this.txtApellido_Mo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApellido_Mo_KeyPress);
            // 
            // txtNombre_Mo
            // 
            this.txtNombre_Mo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre_Mo.Location = new System.Drawing.Point(220, 63);
            this.txtNombre_Mo.Name = "txtNombre_Mo";
            this.txtNombre_Mo.Size = new System.Drawing.Size(191, 31);
            this.txtNombre_Mo.TabIndex = 51;
            this.txtNombre_Mo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_Mo_KeyPress);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelar.BackgroundImage")));
            this.btnCancelar.Location = new System.Drawing.Point(163, 304);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(128, 128);
            this.btnCancelar.TabIndex = 50;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnModificar_Usuario
            // 
            this.btnModificar_Usuario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnModificar_Usuario.BackgroundImage")));
            this.btnModificar_Usuario.Location = new System.Drawing.Point(18, 304);
            this.btnModificar_Usuario.Name = "btnModificar_Usuario";
            this.btnModificar_Usuario.Size = new System.Drawing.Size(128, 128);
            this.btnModificar_Usuario.TabIndex = 49;
            this.btnModificar_Usuario.UseVisualStyleBackColor = true;
            this.btnModificar_Usuario.Click += new System.EventHandler(this.btnModificar_Usuario_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 36);
            this.label1.TabIndex = 48;
            this.label1.Text = "Modificar usuarios";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(15, 254);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 29);
            this.label7.TabIndex = 64;
            this.label7.Text = "Tipo";
            // 
            // cmbTipo_Mo
            // 
            this.cmbTipo_Mo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTipo_Mo.FormattingEnabled = true;
            this.cmbTipo_Mo.Items.AddRange(new object[] {
            "Administrador",
            "Cajero",
            "Inventarios"});
            this.cmbTipo_Mo.Location = new System.Drawing.Point(220, 251);
            this.cmbTipo_Mo.Name = "cmbTipo_Mo";
            this.cmbTipo_Mo.Size = new System.Drawing.Size(191, 32);
            this.cmbTipo_Mo.TabIndex = 66;
            this.cmbTipo_Mo.Text = "Tipo";
            // 
            // Modificar_Usuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 443);
            this.Controls.Add(this.cmbTipo_Mo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtContrasena_Mo);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.btnAyuda);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtNombreUsuario_Mo);
            this.Controls.Add(this.txtDireccion_Mo);
            this.Controls.Add(this.txtApellido_Mo);
            this.Controls.Add(this.txtNombre_Mo);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnModificar_Usuario);
            this.Controls.Add(this.label1);
            this.Name = "Modificar_Usuario";
            this.Text = "Modificar_Usuario";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtContrasena_Mo;
        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.TextBox txtNombreUsuario_Mo;
        public System.Windows.Forms.TextBox txtDireccion_Mo;
        public System.Windows.Forms.TextBox txtApellido_Mo;
        public System.Windows.Forms.TextBox txtNombre_Mo;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnModificar_Usuario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.ComboBox cmbTipo_Mo;
    }
}