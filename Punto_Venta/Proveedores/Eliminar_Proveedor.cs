﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Punto_Venta
{
    public partial class Eliminar_Proveedor : Form
    {
        public Eliminar_Proveedor()
        {
            InitializeComponent();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Proveedores Open = new Proveedores();
            Open.ShowDialog();
        }

        private void btnEliminar_Proveedor_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Esta seguro que desea eliminar el proveedor??", "Esta seguro?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                int resultado = Met_Proveedores.Eliminar(txtIdProveedor.Text);

                if (resultado > 0)
                {

                    MessageBox.Show("Proveedor Eliminado Correctamente", "Proveedor Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Hide();
                    Proveedores Abrir = new Proveedores();
                    Abrir.ShowDialog();
                    //limpiar();
                    //btnEliminar.Enabled = false;
                    //btnModificar.Enabled = false;
                    //btnGuardar.Enabled = true;
                }

                else
                {
                    MessageBox.Show("No se pudo Eliminar el proveedor", "Ocurrio un error!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
                MessageBox.Show("Se cancelo la eliminacion", "Cancelado");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
