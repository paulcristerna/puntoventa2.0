﻿namespace Punto_Venta
{
    partial class Proveedores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Proveedores));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button5 = new System.Windows.Forms.Button();
            this.btnBuscar_Proveedor = new System.Windows.Forms.Button();
            this.btnEliminar_Proveedor = new System.Windows.Forms.Button();
            this.btnModificar_Proveedor = new System.Windows.Forms.Button();
            this.btnRegistrar_Proveedor = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnAtras = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(728, 227);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(223, 217);
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            // 
            // button5
            // 
            this.button5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button5.BackgroundImage")));
            this.button5.Location = new System.Drawing.Point(823, 1);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(128, 128);
            this.button5.TabIndex = 26;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // btnBuscar_Proveedor
            // 
            this.btnBuscar_Proveedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBuscar_Proveedor.BackgroundImage")));
            this.btnBuscar_Proveedor.Location = new System.Drawing.Point(403, 1);
            this.btnBuscar_Proveedor.Name = "btnBuscar_Proveedor";
            this.btnBuscar_Proveedor.Size = new System.Drawing.Size(128, 128);
            this.btnBuscar_Proveedor.TabIndex = 25;
            this.btnBuscar_Proveedor.UseVisualStyleBackColor = true;
            this.btnBuscar_Proveedor.Click += new System.EventHandler(this.btnBuscar_Proveedor_Click);
            // 
            // btnEliminar_Proveedor
            // 
            this.btnEliminar_Proveedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEliminar_Proveedor.BackgroundImage")));
            this.btnEliminar_Proveedor.Location = new System.Drawing.Point(269, 1);
            this.btnEliminar_Proveedor.Name = "btnEliminar_Proveedor";
            this.btnEliminar_Proveedor.Size = new System.Drawing.Size(128, 128);
            this.btnEliminar_Proveedor.TabIndex = 24;
            this.btnEliminar_Proveedor.UseVisualStyleBackColor = true;
            this.btnEliminar_Proveedor.Click += new System.EventHandler(this.btnEliminar_Proveedor_Click);
            // 
            // btnModificar_Proveedor
            // 
            this.btnModificar_Proveedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnModificar_Proveedor.BackgroundImage")));
            this.btnModificar_Proveedor.Location = new System.Drawing.Point(135, 1);
            this.btnModificar_Proveedor.Name = "btnModificar_Proveedor";
            this.btnModificar_Proveedor.Size = new System.Drawing.Size(128, 128);
            this.btnModificar_Proveedor.TabIndex = 23;
            this.btnModificar_Proveedor.UseVisualStyleBackColor = true;
            this.btnModificar_Proveedor.Click += new System.EventHandler(this.btnModificar_Proveedor_Click);
            // 
            // btnRegistrar_Proveedor
            // 
            this.btnRegistrar_Proveedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRegistrar_Proveedor.BackgroundImage")));
            this.btnRegistrar_Proveedor.Location = new System.Drawing.Point(1, 1);
            this.btnRegistrar_Proveedor.Name = "btnRegistrar_Proveedor";
            this.btnRegistrar_Proveedor.Size = new System.Drawing.Size(128, 128);
            this.btnRegistrar_Proveedor.TabIndex = 22;
            this.btnRegistrar_Proveedor.UseVisualStyleBackColor = true;
            this.btnRegistrar_Proveedor.Click += new System.EventHandler(this.btnRegistrar_Proveedor_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 144);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(700, 404);
            this.dataGridView1.TabIndex = 21;
            // 
            // btnAtras
            // 
            this.btnAtras.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAtras.BackgroundImage")));
            this.btnAtras.Location = new System.Drawing.Point(689, 1);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(128, 128);
            this.btnAtras.TabIndex = 28;
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // Proveedores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 561);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.btnBuscar_Proveedor);
            this.Controls.Add(this.btnEliminar_Proveedor);
            this.Controls.Add(this.btnModificar_Proveedor);
            this.Controls.Add(this.btnRegistrar_Proveedor);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Proveedores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Proveedores";
            this.Load += new System.EventHandler(this.Proveedores_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnBuscar_Proveedor;
        private System.Windows.Forms.Button btnEliminar_Proveedor;
        private System.Windows.Forms.Button btnModificar_Proveedor;
        private System.Windows.Forms.Button btnRegistrar_Proveedor;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnAtras;
    }
}