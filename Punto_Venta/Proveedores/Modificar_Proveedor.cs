﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Punto_Venta
{
    public partial class Modificar_Proveedor : Form
    {
        public Modificar_Proveedor()
        {
            InitializeComponent();
        }

        private void btnModificar_Proveedor_Click(object sender, EventArgs e)
        {
            try
            {
                Pro_Proveedores Pro_Proveedores = new Pro_Proveedores();
                if (txtNombre_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo nombre", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtNombre_Mo.Focus();
                    return;
                }
                if (txtApellido_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo apellido", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtApellido_Mo.Focus();
                    return;
                }
                if (txtDireccion_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo direccion", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDireccion_Mo.Focus();
                    return;
                }
                if (txtRazonSocial_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo telefono de razon social", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtRazonSocial_Mo.Focus();
                    return;
                }
                if (txtTelefonoOficina_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo telefono de oficina", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtTelefonoOficina_Mo.Focus();
                    return;
                }
                if (txtCelular_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo celular", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCelular_Mo.Focus();
                    return;
                }
                if (txtEmail_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo email", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEmail_Mo.Focus();
                    return;
                }
                else
                    Pro_Proveedores.Id_Proveedor = Convert.ToInt16(txtIdProveedor_Mo.Text);
                    Pro_Proveedores.Nombre = txtNombre_Mo.Text;
                    Pro_Proveedores.Apellido = txtApellido_Mo.Text;
                    Pro_Proveedores.Razon_Social = txtRazonSocial_Mo.Text;
                    Pro_Proveedores.Direccion = txtDireccion_Mo.Text;
                    Pro_Proveedores.TelefonoOficina = txtTelefonoOficina_Mo.Text;
                    Pro_Proveedores.Celular = txtCelular_Mo.Text;
                    Pro_Proveedores.Email = txtEmail_Mo.Text;
                    Met_Proveedores.Modificar(Pro_Proveedores);
                    MessageBox.Show("Datos Guardados Corerectamente", "Datos Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpiar();
            }
            catch
            {
                MessageBox.Show("No se pudieron Guardar lo datos", "Error al Guardar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        void limpiar()
        {
            txtIdProveedor_Mo.Clear();
            txtNombre_Mo.Clear();
            txtApellido_Mo.Clear();
            txtRazonSocial_Mo.Clear();
            txtDireccion_Mo.Clear();
            txtTelefonoOficina_Mo.Clear();
            txtCelular_Mo.Clear();
            txtEmail_Mo.Clear();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Proveedores Abrir = new Proveedores();
            Abrir.ShowDialog();
        }

        private void txtNombre_Mo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Letras(e);
        }

        private void txtApellido_Mo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Letras(e);
        }

        private void txtTelefonoOficina_Mo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Numeros(e);
        }

        private void txtCelular_Mo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Numeros(e);
        }
    }
}
