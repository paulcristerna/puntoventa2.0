﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace Punto_Venta
{
    class Met_Proveedores
    {
        public static DataTable CargarProveedores()
        {
            DataTable retorno = new DataTable();
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                string ConsultaUsuarios = "SELECT * FROM Proveedores";
                MySqlCommand Comando = new MySqlCommand(ConsultaUsuarios, conexion);
                MySqlDataAdapter Adaptador = new MySqlDataAdapter(Comando);
                Adaptador.Fill(retorno);
            }

            return retorno;
        }

        public static int Agregar(Pro_Proveedores pProveedor)
        {
            int retorno = 0;
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                MySqlCommand Comando = new MySqlCommand(string.Format("Insert Into Proveedores (Nombre, Apellido, Razon_Social, Direccion, TelefonoOficina, Celular, Email) values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}')",
                    pProveedor.Nombre, pProveedor.Apellido, pProveedor.Razon_Social, pProveedor.Direccion, pProveedor.TelefonoOficina, pProveedor.Celular, pProveedor.Email), conexion);

                retorno = Comando.ExecuteNonQuery();
                conexion.Close();

            }
            return retorno;
        }

        public static int Modificar(Pro_Proveedores pProveedor)
        {
            int retorno = 0;
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                MySqlCommand comando = new MySqlCommand(string.Format("Update Proveedores set Nombre='{0}', Apellido='{1}', Razon_Social='{2}', Direccion='{3}', TelefonoOficina='{4}', Celular='{5}', Email='{6}' where Id_Proveedor='{7}'",
                    pProveedor.Nombre, pProveedor.Apellido, pProveedor.Razon_Social, pProveedor.Direccion, pProveedor.TelefonoOficina, pProveedor.Celular, pProveedor.Email, pProveedor.Id_Proveedor), conexion);

                retorno = comando.ExecuteNonQuery();
                conexion.Close();
            }
            return retorno;
        }

        public static int Eliminar(String pProveedor)
        {
            int retorno = 0;
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                MySqlCommand comando = new MySqlCommand(string.Format("Delete from Proveedores where Id_Proveedor='{0}'", pProveedor), conexion);
                retorno = comando.ExecuteNonQuery();
                conexion.Close();
            }
            return retorno;
        }
    }
}
