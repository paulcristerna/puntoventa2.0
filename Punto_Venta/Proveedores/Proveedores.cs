﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Punto_Venta
{
    public partial class Proveedores : Form
    {
        public Proveedores()
        {
            InitializeComponent();
        }

        private void btnRegistrar_Proveedor_Click(object sender, EventArgs e)
        {
            this.Hide();
            Registro_Proveedor Open = new Registro_Proveedor();
            Open.ShowDialog();
        }

        private void btnModificar_Proveedor_Click(object sender, EventArgs e)
        {
            Modificar_Proveedor Form = new Modificar_Proveedor();
            Form.txtIdProveedor_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[0].Value);
            Form.txtNombre_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[1].Value);
            Form.txtApellido_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[2].Value);
            Form.txtRazonSocial_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[3].Value);
            Form.txtDireccion_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[4].Value);
            Form.txtTelefonoOficina_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[5].Value);
            Form.txtCelular_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[6].Value);
            Form.txtEmail_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[7].Value);
            this.Hide();
            Form.ShowDialog();
        }

        private void btnEliminar_Proveedor_Click(object sender, EventArgs e)
        {
            Eliminar_Proveedor Form = new Eliminar_Proveedor();
            Form.txtIdProveedor.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[0].Value);
            Form.txtNombre.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[1].Value);
            Form.txtApellido.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[2].Value);
            Form.txtRazonSocial.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[3].Value);
            Form.txtDireccion.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[4].Value);
            Form.txtTelefonoOficina.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[5].Value);
            Form.txtCelular.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[6].Value);
            Form.txtEmail.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[7].Value);
            this.Hide();
            Form.ShowDialog();
        }

        private void btnBuscar_Proveedor_Click(object sender, EventArgs e)
        {

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Menu_Principal Open = new Menu_Principal();
            Open.ShowDialog();
        }

        private void Proveedores_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Met_Proveedores.CargarProveedores();
        }
    }
}
