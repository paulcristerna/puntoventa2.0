﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Punto_Venta
{
    public partial class Eliminar_Cliente : Form
    {
        public Eliminar_Cliente()
        {
            InitializeComponent();
        }

        private void btnEliminar_Cliente_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Esta seguro que desea eliminar el cliente??", "Esta seguro?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                int resultado = Met_Clientes.Eliminar(txtIdCliente.Text);

                if (resultado > 0)
                {

                    MessageBox.Show("Cliente Eliminado Correctamente", "Cliente Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Hide();
                    Clientes Abrir = new Clientes();
                    Abrir.ShowDialog();
                    //limpiar();
                    //btnEliminar.Enabled = false;
                    //btnModificar.Enabled = false;
                    //btnGuardar.Enabled = true;
                }

                else
                {
                    MessageBox.Show("No se pudo Eliminar el cliente", "Ocurrio un error!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
                MessageBox.Show("Se cancelo la eliminacion", "Cancelado");
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Clientes Open = new Clientes();
            Open.ShowDialog();
        }
    }
}
