﻿namespace Punto_Venta
{
    partial class Modificar_Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Modificar_Cliente));
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCelular_Mo = new System.Windows.Forms.TextBox();
            this.btnAtras = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtTelefonoCasa_Mo = new System.Windows.Forms.TextBox();
            this.txtDireccion_Mo = new System.Windows.Forms.TextBox();
            this.txtApellido_Mo = new System.Windows.Forms.TextBox();
            this.txtNombre_Mo = new System.Windows.Forms.TextBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnModificar_Cliente = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFechaNacimiento_Mo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtEmail_Mo = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtIdCliente_Mo = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 247);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 29);
            this.label6.TabIndex = 79;
            this.label6.Text = "Celular";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 210);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(179, 29);
            this.label5.TabIndex = 78;
            this.label5.Text = "Telefono de casa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 173);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 29);
            this.label4.TabIndex = 77;
            this.label4.Text = "Direccion";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 29);
            this.label3.TabIndex = 76;
            this.label3.Text = "Apellidos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 29);
            this.label2.TabIndex = 75;
            this.label2.Text = "Nombre";
            // 
            // txtCelular_Mo
            // 
            this.txtCelular_Mo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCelular_Mo.Location = new System.Drawing.Point(243, 245);
            this.txtCelular_Mo.Name = "txtCelular_Mo";
            this.txtCelular_Mo.Size = new System.Drawing.Size(191, 31);
            this.txtCelular_Mo.TabIndex = 74;
            this.txtCelular_Mo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCelular_Mo_KeyPress);
            // 
            // btnAtras
            // 
            this.btnAtras.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAtras.BackgroundImage")));
            this.btnAtras.Location = new System.Drawing.Point(581, 9);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(128, 128);
            this.btnAtras.TabIndex = 73;
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAyuda.BackgroundImage")));
            this.btnAyuda.Location = new System.Drawing.Point(715, 9);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(128, 128);
            this.btnAyuda.TabIndex = 72;
            this.btnAyuda.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(620, 163);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(223, 217);
            this.pictureBox1.TabIndex = 71;
            this.pictureBox1.TabStop = false;
            // 
            // txtTelefonoCasa_Mo
            // 
            this.txtTelefonoCasa_Mo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefonoCasa_Mo.Location = new System.Drawing.Point(243, 208);
            this.txtTelefonoCasa_Mo.Name = "txtTelefonoCasa_Mo";
            this.txtTelefonoCasa_Mo.Size = new System.Drawing.Size(191, 31);
            this.txtTelefonoCasa_Mo.TabIndex = 70;
            this.txtTelefonoCasa_Mo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefonoCasa_Mo_KeyPress);
            // 
            // txtDireccion_Mo
            // 
            this.txtDireccion_Mo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDireccion_Mo.Location = new System.Drawing.Point(243, 171);
            this.txtDireccion_Mo.Name = "txtDireccion_Mo";
            this.txtDireccion_Mo.Size = new System.Drawing.Size(191, 31);
            this.txtDireccion_Mo.TabIndex = 69;
            // 
            // txtApellido_Mo
            // 
            this.txtApellido_Mo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellido_Mo.Location = new System.Drawing.Point(243, 133);
            this.txtApellido_Mo.Name = "txtApellido_Mo";
            this.txtApellido_Mo.Size = new System.Drawing.Size(191, 31);
            this.txtApellido_Mo.TabIndex = 68;
            this.txtApellido_Mo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApellido_Mo_KeyPress);
            // 
            // txtNombre_Mo
            // 
            this.txtNombre_Mo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre_Mo.Location = new System.Drawing.Point(243, 93);
            this.txtNombre_Mo.Name = "txtNombre_Mo";
            this.txtNombre_Mo.Size = new System.Drawing.Size(191, 31);
            this.txtNombre_Mo.TabIndex = 67;
            this.txtNombre_Mo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_Mo_KeyPress);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelar.BackgroundImage")));
            this.btnCancelar.Location = new System.Drawing.Point(163, 370);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(128, 128);
            this.btnCancelar.TabIndex = 66;
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnModificar_Cliente
            // 
            this.btnModificar_Cliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnModificar_Cliente.BackgroundImage")));
            this.btnModificar_Cliente.Location = new System.Drawing.Point(18, 370);
            this.btnModificar_Cliente.Name = "btnModificar_Cliente";
            this.btnModificar_Cliente.Size = new System.Drawing.Size(128, 128);
            this.btnModificar_Cliente.TabIndex = 65;
            this.btnModificar_Cliente.UseVisualStyleBackColor = true;
            this.btnModificar_Cliente.Click += new System.EventHandler(this.btnModificar_Cliente_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 36);
            this.label1.TabIndex = 64;
            this.label1.Text = "Modificar clientes";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(15, 329);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(222, 29);
            this.label7.TabIndex = 81;
            this.label7.Text = "Fecha de Nacimiento";
            // 
            // txtFechaNacimiento_Mo
            // 
            this.txtFechaNacimiento_Mo.Enabled = false;
            this.txtFechaNacimiento_Mo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechaNacimiento_Mo.Location = new System.Drawing.Point(243, 327);
            this.txtFechaNacimiento_Mo.Name = "txtFechaNacimiento_Mo";
            this.txtFechaNacimiento_Mo.Size = new System.Drawing.Size(191, 31);
            this.txtFechaNacimiento_Mo.TabIndex = 80;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(15, 287);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 29);
            this.label8.TabIndex = 83;
            this.label8.Text = "Email";
            // 
            // txtEmail_Mo
            // 
            this.txtEmail_Mo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail_Mo.Location = new System.Drawing.Point(243, 285);
            this.txtEmail_Mo.Name = "txtEmail_Mo";
            this.txtEmail_Mo.Size = new System.Drawing.Size(191, 31);
            this.txtEmail_Mo.TabIndex = 82;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(13, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 29);
            this.label9.TabIndex = 85;
            this.label9.Text = "Id Cliente";
            // 
            // txtIdCliente_Mo
            // 
            this.txtIdCliente_Mo.Enabled = false;
            this.txtIdCliente_Mo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdCliente_Mo.Location = new System.Drawing.Point(243, 53);
            this.txtIdCliente_Mo.Name = "txtIdCliente_Mo";
            this.txtIdCliente_Mo.Size = new System.Drawing.Size(191, 31);
            this.txtIdCliente_Mo.TabIndex = 84;
            // 
            // Modificar_Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 510);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtIdCliente_Mo);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtEmail_Mo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtFechaNacimiento_Mo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtCelular_Mo);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.btnAyuda);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtTelefonoCasa_Mo);
            this.Controls.Add(this.txtDireccion_Mo);
            this.Controls.Add(this.txtApellido_Mo);
            this.Controls.Add(this.txtNombre_Mo);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnModificar_Cliente);
            this.Controls.Add(this.label1);
            this.Name = "Modificar_Cliente";
            this.Text = "Modificar_Cliente";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtCelular_Mo;
        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.TextBox txtTelefonoCasa_Mo;
        public System.Windows.Forms.TextBox txtDireccion_Mo;
        public System.Windows.Forms.TextBox txtApellido_Mo;
        public System.Windows.Forms.TextBox txtNombre_Mo;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnModificar_Cliente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtFechaNacimiento_Mo;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtEmail_Mo;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtIdCliente_Mo;
    }
}