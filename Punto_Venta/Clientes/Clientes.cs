﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Punto_Venta
{
    public partial class Clientes : Form
    {
        public Clientes()
        {
            InitializeComponent();
        }

        private void btnRegistrar_Cliente_Click(object sender, EventArgs e)
        {
            this.Hide();
            Registro_Cliente Open = new Registro_Cliente();
            Open.ShowDialog();
        }

        private void btnModificar_Cliente_Click(object sender, EventArgs e)
        {
            Modificar_Cliente Form = new Modificar_Cliente();
            Form.txtIdCliente_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[0].Value);
            Form.txtNombre_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[1].Value);
            Form.txtApellido_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[2].Value);
            Form.txtDireccion_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[3].Value);
            Form.txtTelefonoCasa_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[4].Value);
            Form.txtCelular_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[5].Value);
            Form.txtEmail_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[6].Value);
            Form.txtFechaNacimiento_Mo.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[7].Value);
            this.Hide();
            Form.ShowDialog();
        }

        private void btnEliminar_Cliente_Click(object sender, EventArgs e)
        {
            Eliminar_Cliente Form = new Eliminar_Cliente();
            Form.txtIdCliente.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[0].Value);
            Form.txtNombre.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[1].Value);
            Form.txtApellido.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[2].Value);
            Form.txtDireccion.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[3].Value);
            Form.txtTelefonoCasa.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[4].Value);
            Form.txtCelular.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[5].Value);
            Form.txtEmail.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[6].Value);
            Form.txtFechaNacimiento.Text = Convert.ToString(this.dataGridView1.CurrentRow.Cells[7].Value);
            this.Hide();
            Form.ShowDialog();
        }

        private void btnBuscar_Cliente_Click(object sender, EventArgs e)
        {

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Menu_Principal Open = new Menu_Principal();
            Open.ShowDialog();
        }

        private void Clientes_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Met_Clientes.CargarClientes();
        }


    }
}
