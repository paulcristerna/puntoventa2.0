﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices; //Servicio para activar user32.dll

namespace Punto_Venta
{
    public partial class Registro_Cliente : Form
    {
        //Servicio de PlaceHolder en textboxs
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern Int32
                SendMessage(
                                IntPtr hWnd,
                                int msg,
                                int wParam,
                                [MarshalAs(UnmanagedType.LPWStr)]string lParam
                            );
        private const int EM_SETCUEBANNER = 0x1501;

        public Registro_Cliente()
        {
            InitializeComponent();
            //Aqui se definen los mensajes que apareceran en los textboxs de placeholder
            SendMessage(txtNombre.Handle, EM_SETCUEBANNER, 0, "Nombre");
            SendMessage(txtApellido.Handle, EM_SETCUEBANNER, 0, "Apellido");
            SendMessage(txtDireccion.Handle, EM_SETCUEBANNER, 0, "Direccion");
            SendMessage(txtTelefonoCasa.Handle, EM_SETCUEBANNER, 0, "Telefono de casa");
            SendMessage(txtCelular.Handle, EM_SETCUEBANNER, 0, "Celular");
            SendMessage(txtEmail.Handle, EM_SETCUEBANNER, 0, "Email");
        }

        private void btnGuardar_Cliente_Click(object sender, EventArgs e)
        {
            try
            {
                Pro_Clientes Pro_Clientes = new Pro_Clientes();
                if (txtNombre.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo nombre", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtNombre.Focus();
                    return;
                }
                if (txtApellido.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo apellido", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtApellido.Focus();
                    return;
                }
                if (txtDireccion.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo direccion", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDireccion.Focus();
                    return;
                }
                if (txtTelefonoCasa.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo telefono de casa", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtTelefonoCasa.Focus();
                    return;
                }
                if (txtCelular.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo celular", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCelular.Focus();
                    return;
                }
                if (txtEmail.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo email", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEmail.Focus();
                    return;
                }
                else
                    Pro_Clientes.Nombre = txtNombre.Text;
                    Pro_Clientes.Apellido = txtApellido.Text;
                    Pro_Clientes.Direccion = txtDireccion.Text;
                    Pro_Clientes.TelefonoCasa = txtTelefonoCasa.Text;
                    Pro_Clientes.Celular = txtCelular.Text;
                    Pro_Clientes.Email = txtEmail.Text;
                    Pro_Clientes.Fecha_Nacimiento = string.Format("{0:yyyy-MM-dd}", Fecha_Nacimiento.Value);
                    Met_Clientes.Agregar(Pro_Clientes);
                    MessageBox.Show("Datos Guardados Corerectamente", "Datos Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpiar();
            }
            catch
            {
                MessageBox.Show("No se pudieron Guardar lo datos", "Error al Guardar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        void limpiar()
        {
            txtNombre.Clear();
            txtApellido.Clear();
            txtDireccion.Clear();
            txtTelefonoCasa.Clear();
            txtCelular.Clear();
            txtEmail.Clear();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Clientes Open = new Clientes();
            Open.ShowDialog();
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Letras(e);
        }

        private void txtApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Letras(e);
        }

        private void txtTelefonoCasa_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Numeros(e);
        }

        private void txtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Numeros(e);
        }
    }
}
