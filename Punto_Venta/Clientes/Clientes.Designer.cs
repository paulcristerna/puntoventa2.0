﻿namespace Punto_Venta
{
    partial class Clientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Clientes));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button5 = new System.Windows.Forms.Button();
            this.btnBuscar_Cliente = new System.Windows.Forms.Button();
            this.btnEliminar_Cliente = new System.Windows.Forms.Button();
            this.btnModificar_Cliente = new System.Windows.Forms.Button();
            this.btnRegistrar_Cliente = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnAtras = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(729, 227);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(223, 217);
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // button5
            // 
            this.button5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button5.BackgroundImage")));
            this.button5.Location = new System.Drawing.Point(824, 1);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(128, 128);
            this.button5.TabIndex = 19;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // btnBuscar_Cliente
            // 
            this.btnBuscar_Cliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBuscar_Cliente.BackgroundImage")));
            this.btnBuscar_Cliente.Enabled = false;
            this.btnBuscar_Cliente.Location = new System.Drawing.Point(404, 1);
            this.btnBuscar_Cliente.Name = "btnBuscar_Cliente";
            this.btnBuscar_Cliente.Size = new System.Drawing.Size(128, 128);
            this.btnBuscar_Cliente.TabIndex = 18;
            this.btnBuscar_Cliente.UseVisualStyleBackColor = true;
            this.btnBuscar_Cliente.Click += new System.EventHandler(this.btnBuscar_Cliente_Click);
            // 
            // btnEliminar_Cliente
            // 
            this.btnEliminar_Cliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEliminar_Cliente.BackgroundImage")));
            this.btnEliminar_Cliente.Location = new System.Drawing.Point(270, 1);
            this.btnEliminar_Cliente.Name = "btnEliminar_Cliente";
            this.btnEliminar_Cliente.Size = new System.Drawing.Size(128, 128);
            this.btnEliminar_Cliente.TabIndex = 17;
            this.btnEliminar_Cliente.UseVisualStyleBackColor = true;
            this.btnEliminar_Cliente.Click += new System.EventHandler(this.btnEliminar_Cliente_Click);
            // 
            // btnModificar_Cliente
            // 
            this.btnModificar_Cliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnModificar_Cliente.BackgroundImage")));
            this.btnModificar_Cliente.Location = new System.Drawing.Point(136, 1);
            this.btnModificar_Cliente.Name = "btnModificar_Cliente";
            this.btnModificar_Cliente.Size = new System.Drawing.Size(128, 128);
            this.btnModificar_Cliente.TabIndex = 16;
            this.btnModificar_Cliente.UseVisualStyleBackColor = true;
            this.btnModificar_Cliente.Click += new System.EventHandler(this.btnModificar_Cliente_Click);
            // 
            // btnRegistrar_Cliente
            // 
            this.btnRegistrar_Cliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRegistrar_Cliente.BackgroundImage")));
            this.btnRegistrar_Cliente.Location = new System.Drawing.Point(2, 1);
            this.btnRegistrar_Cliente.Name = "btnRegistrar_Cliente";
            this.btnRegistrar_Cliente.Size = new System.Drawing.Size(128, 128);
            this.btnRegistrar_Cliente.TabIndex = 15;
            this.btnRegistrar_Cliente.UseVisualStyleBackColor = true;
            this.btnRegistrar_Cliente.Click += new System.EventHandler(this.btnRegistrar_Cliente_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 144);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(700, 404);
            this.dataGridView1.TabIndex = 14;
            // 
            // btnAtras
            // 
            this.btnAtras.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAtras.BackgroundImage")));
            this.btnAtras.Location = new System.Drawing.Point(690, 1);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(128, 128);
            this.btnAtras.TabIndex = 21;
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // Clientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 561);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.btnBuscar_Cliente);
            this.Controls.Add(this.btnEliminar_Cliente);
            this.Controls.Add(this.btnModificar_Cliente);
            this.Controls.Add(this.btnRegistrar_Cliente);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Clientes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Clientes";
            this.Load += new System.EventHandler(this.Clientes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnBuscar_Cliente;
        private System.Windows.Forms.Button btnEliminar_Cliente;
        private System.Windows.Forms.Button btnModificar_Cliente;
        private System.Windows.Forms.Button btnRegistrar_Cliente;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnAtras;
    }
}