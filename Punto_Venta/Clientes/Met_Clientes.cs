﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace Punto_Venta
{
    class Met_Clientes
    {
        public static DataTable CargarClientes()
        {
            DataTable retorno = new DataTable();
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                string ConsultaUsuarios = "SELECT * FROM Clientes";
                MySqlCommand Comando = new MySqlCommand(ConsultaUsuarios, conexion);
                MySqlDataAdapter Adaptador = new MySqlDataAdapter(Comando);
                Adaptador.Fill(retorno);
            }

            return retorno;
        }

        public static int Agregar(Pro_Clientes pCliente)
        {
            int retorno = 0;
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                MySqlCommand Comando = new MySqlCommand(string.Format("Insert Into Clientes (Nombre, Apellido, Direccion, TelefonoCasa, Celular, Email, FechaNacimiento) values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}')",
                    pCliente.Nombre, pCliente.Apellido, pCliente.Direccion, pCliente.TelefonoCasa, pCliente.Celular, pCliente.Email, pCliente.Fecha_Nacimiento), conexion);

                retorno = Comando.ExecuteNonQuery();
                conexion.Close();

            }
            return retorno;
        }

        public static int Modificar(Pro_Clientes pCliente)
        {
            int retorno = 0;
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                MySqlCommand comando = new MySqlCommand(string.Format("Update Clientes set Nombre='{0}', Apellido='{1}', Direccion='{2}', TelefonoCasa='{3}', Celular='{4}', Email='{5}' where Id_Cliente='{6}'",
                    pCliente.Nombre, pCliente.Apellido, pCliente.Direccion, pCliente.TelefonoCasa, pCliente.Celular,pCliente.Email, pCliente.Id_Cliente), conexion);

                retorno = comando.ExecuteNonQuery();
                conexion.Close();
            }
            return retorno;
        }

        public static int Eliminar(String pCliente)
        {
            int retorno = 0;
            using (MySqlConnection conexion = Conexion.MiConexion())
            {
                MySqlCommand comando = new MySqlCommand(string.Format("Delete from Clientes where Id_Cliente='{0}'", pCliente), conexion);
                retorno = comando.ExecuteNonQuery();
                conexion.Close();
            }
            return retorno;
        }
    }
}
