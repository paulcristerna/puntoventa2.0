﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Punto_Venta
{
    public partial class Modificar_Cliente : Form
    {
        public Modificar_Cliente()
        {
            InitializeComponent();
        }

        private void btnModificar_Cliente_Click(object sender, EventArgs e)
        {
            try
            {
                Pro_Clientes Pro_Clientes = new Pro_Clientes();
                if (txtNombre_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo nombre", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtNombre_Mo.Focus();
                    return;
                }
                if (txtApellido_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo apellido", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtApellido_Mo.Focus();
                    return;
                }
                if (txtDireccion_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo direccion", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDireccion_Mo.Focus();
                    return;
                }
                if (txtTelefonoCasa_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo telefono de casa", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtTelefonoCasa_Mo.Focus();
                    return;
                }
                if (txtCelular_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo celular", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCelular_Mo.Focus();
                    return;
                }
                if (txtEmail_Mo.Text.Length == 0)
                {
                    MessageBox.Show("Error en campo email", "Campos Obligatorio, verifique", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEmail_Mo.Focus();
                    return;
                }
                else
                    Pro_Clientes.Id_Cliente = Convert.ToInt16(txtIdCliente_Mo.Text);
                    Pro_Clientes.Nombre = txtNombre_Mo.Text;
                    Pro_Clientes.Apellido = txtApellido_Mo.Text;
                    Pro_Clientes.Direccion = txtDireccion_Mo.Text;
                    Pro_Clientes.TelefonoCasa = txtTelefonoCasa_Mo.Text;
                    Pro_Clientes.Celular = txtCelular_Mo.Text;
                    Pro_Clientes.Email = txtEmail_Mo.Text;
                    Pro_Clientes.Fecha_Nacimiento = txtFechaNacimiento_Mo.Text;
                    Met_Clientes.Modificar(Pro_Clientes);
                    MessageBox.Show("Datos Guardados Corerectamente", "Datos Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpiar();
            }
            catch
            {
                MessageBox.Show("No se pudieron Guardar lo datos", "Error al Guardar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        void limpiar()
        {
            txtIdCliente_Mo.Clear();
            txtNombre_Mo.Clear();
            txtApellido_Mo.Clear();
            txtDireccion_Mo.Clear();
            txtTelefonoCasa_Mo.Clear();
            txtCelular_Mo.Clear();
            txtEmail_Mo.Clear();
            txtFechaNacimiento_Mo.Clear();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Clientes Abrir = new Clientes();
            Abrir.ShowDialog();
        }

        private void txtNombre_Mo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Letras(e);
        }

        private void txtApellido_Mo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Letras(e);
        }

        private void txtTelefonoCasa_Mo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Numeros(e);
        }

        private void txtCelular_Mo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.Numeros(e);
        }
    }
}
